import * as util from '../util/util.js';

var apiUrl = util.apiUrl;

$(function() {
    $('a[href="#links"]').on('click', function(event) {
        util.clearAllData();
        $.ajax({
            url: apiUrl + 'get.php?get=categories',
            success: function(data, textStatus, jqXHR) {
                var categories = JSON.parse(data);
                if (!categories) {
                    util.showInfoMessage('Категории не найдены');
                    return;
                }
                $.ajax({
                    url: apiUrl + 'get.php?get=criteria',
                    success: function(data, textStatus, jqXHR) {
                        var criteria = JSON.parse(data);
                        if (!criteria) {
                            util.showInfoMessage('Критерии не найдены');
                            return;
                        }
                        $.ajax({
                            url: apiUrl + 'get.php?get=answers',
                            success: function(data, textStatus, jqXHR) {
                                var answers = JSON.parse(data);
                                if (!answers) {
                                    util.showInfoMessage('Ответы не найдены');
                                    return;
                                }
                                $.ajax({
                                    url: apiUrl + 'get.php?get=links',
                                    success: function(data, textStatus, jqXHR) {
                                        var links = JSON.parse(data);
                                        if (!links) {
                                            util.showInfoMessage('Связи не найдены');
                                            return;
                                        }
                                        $.ajax({
                                            url: apiUrl + 'get.php?get=subcategories',
                                            success: function(data, textStatus, jqXHR) {
                                                var subCategories = JSON.parse(data);
                                                if (!subCategories) {
                                                    util.showInfoMessage('Подкатегории не найдены');
                                                    // return;
                                                }
                                                $.ajax({
                                                    url: apiUrl + 'get.php?get=meta',
                                                    success: function(data, textStatus, jqXHR) {
                                                        var meta = JSON.parse(data);
                                                        fillMeta(meta);
                                                        fillLinksTable(categories, criteria, answers, links, subCategories);
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    });

    function fillMeta(meta) {
        var metaWrap = $('#links-meta');
        $(metaWrap).empty();
        var heading    = meta.find(function(m, i, a) { return m.meta_key === 'form_heading';    }).meta_value;
        var annotation = meta.find(function(m, i, a) { return m.meta_key === 'form_annotation'; }).meta_value;
        if (annotation === null) annotation = '';
        var metaHtml = 
            '<h4>Заголовок страницы формы:</h4> ' + heading + ' <span id="btnEditFormHeading" class="glyphicon glyphicon-pencil btn-edit" title="Изменить заголовок формы" aria-hidden="true"></span><br/>' +
            '<h4>Аннотация на странице формы:</h4>' +
            '<textarea name="annotation" cols="auto" rows="5">' + annotation + '</textarea><br>' +
            '<a id="btnEditFormAnnotation" class="pull-right">Сохранить изменения</a>';
        $(metaWrap).append(metaHtml);
        $('#btnEditFormHeading').click(function(event) {
            var newFormHeading = prompt('Введите новый заголовок формы', '');
            if (newFormHeading !== null && newFormHeading !== '') {
                $.ajax({
                    url: apiUrl + 'post.php',
                    type: 'POST',
                    data: {
                        section: 'meta',
                        method: 'edit',
                        meta_key: 'form_heading',
                        meta_value: newFormHeading
                    },
                    success: function(data, textStatus, jqXHR) {
                        if (data === 'success') {
                            util.showSuccessMessage('Заголовок формы изменён');
                            $('a[href="#links"]').trigger('click');
                        }
                    }
                });
            }
        });
        $('#btnEditFormAnnotation').click(function(event) {
            var newFormAnnotation = $('#links textarea').val();
            if (newFormAnnotation === '') newFormAnnotation = 'NULL';
            $.ajax({
                url: apiUrl + 'post.php',
                type: 'POST',
                data: {
                    section: 'meta',
                    method: 'edit',
                    meta_key: 'form_annotation',
                    meta_value: newFormAnnotation
                },
                success: function(data, textStatus, jqXHR) {
                    if (data === 'success') {
                        util.showSuccessMessage('Аннотация на странице формы изменена');
                        $('a[href="#links"]').trigger('click');
                    }
                }
            });
        });
    }

    function fillLinksTable(categories, criteria, answers, links, subCategories) {
        var rootCatId = -1;
        var rootCategoryIndex = categories.findIndex(function (cat, i, a) {
            if (cat.is_root === '1') rootCatId = cat.id;
            return cat.is_root === '1';
        });
        if (rootCategoryIndex > -1) {
            var temp = categories[rootCategoryIndex];
            categories.splice(rootCategoryIndex, 1);
            categories.unshift(temp);
        }
        categories.forEach(function(currentValue, index, array) {
            var categoryHtml = '<div class="col-xs-12"><div data-category-id="' + currentValue.id + 
                               '"><h4>' + currentValue.cat_name + '</h4></div>';
            categoryHtml += '</div>';
            if (index === 0) {
                categoryHtml = categoryHtml.replace('class="col-xs-12"', 'id="rootCat" class="col-xs-12"');
            } else {
                categoryHtml = categoryHtml.replace('class="col-xs-12"', 'class="col-xs-12 hidden"');
            }
            $('#links-wrap').append(categoryHtml);
        });
        $('#rootCat>div').append('<div id="rootWorks"></div>');
        $('#rootCat>div').append('<div id="subWorks" ></div>');
        var categoryHtml = '';
        criteria.forEach(function(currentValue, index, array) {            
            if (currentValue.cat_id === rootCatId) {
                if (currentValue.parent_crit_id === null) {
                    categoryHtml = '<div class="radio"><label><input type="radio" name="cat_' + currentValue.cat_id + '" data-category-id="' + currentValue.cat_id + 
                                   '" data-criterium-id="' + currentValue.id + '" ' +
                                   'value="' + currentValue.crit_name + '">' + currentValue.crit_name + '</label></div>';
                    $('#rootWorks').append(categoryHtml);
                    $('#subWorks').append('<div id="subWorks' + currentValue.id + '"></div>');
                    $('#subWorks' + currentValue.id).hide();
                } else {
                    categoryHtml = '<div class="radio"><label><input type="radio" name="cat_' + currentValue.cat_id + '" data-category-id="' + currentValue.cat_id + 
                                   '" data-criterium-id="' + currentValue.id + '" ' +
                                   'value="' + currentValue.crit_name + '">' + currentValue.crit_name + '</label></div>';
                    $('#subWorks' + currentValue.parent_crit_id).append(categoryHtml);
                }
            } else {
                categoryHtml = '<div class="radio"><label><input type="radio" name="cat_' + currentValue.cat_id + '" data-category-id="' + currentValue.cat_id + 
                                   '" data-criterium-id="' + currentValue.id + '" ' +
                                   'value="' + currentValue.crit_name + '">' + currentValue.crit_name + '</label></div>';
                $('.col-xs-12 div[data-category-id="' + currentValue.cat_id + '"]').append(categoryHtml);
            }
        });
        $('#subWorks>div').append('<div class="back"><a style="cursor: pointer;">Выбрать другой вид работ</a></div>');
        $('.back').click(function (event) {
            $('.col-xs-12').not('#rootCat').addClass('hidden');
            $('#links select').empty();
            var catName = $(this).parent().find('input').first().attr('name');
            $('input[name="' + catName + '"]').attr('checked', false);
            $(this).parent().fadeOut(200, function() {
                $('#rootWorks').fadeIn(200);
            });
        });
        $('#rootCat input').click(function (event) {
            $('.col-xs-12').not('#rootCat').addClass('hidden');
            var input = $(this);
            if ($('#subWorks' + $(this).data('criterium-id')).length === 0 || $('#subWorks' + $(this).data('criterium-id') + '>div').length === 1) {
                subCategories
                    .filter(function (sc, i, a) {
                        return sc.crit_id === input.data('criterium-id').toString();
                    })
                    .forEach(function (sc, i, a) {
                        $('div[data-category-id=' + sc.cat_id + ']').parent().removeClass('hidden');
                    });
            } else {
                $('#rootWorks').fadeOut(200, function() {
                    $('#links select').empty();
                    $('#subWorks' + input.data('criterium-id')).fadeIn(200);
                });
            }
        });
        $('#links input').on('click', function(event) {
            $('#links select').empty();
            var getParameter = '';
            var r3turn = false;
            categories.forEach(function(currentValue, index, array) {
                var curCatId = currentValue.id;
                if (!$('input[data-category-id="' + curCatId + '"]:checked').val() && $('input[data-category-id="' + curCatId + '"]').parents('.hidden').length === 0) {
                    // util.showErrorMessage('Необходимо выбрать вариант в каждой секции');
                    r3turn = true;
                }
                $('input[data-category-id="' + curCatId + '"]').each(function(i) {
                    if ($(this).parents('.hidden').length === 0) { // Если не скрыт
                        if ($(this).prop('checked')) {
                            getParameter += '&cat_' + curCatId + '=' + $(this).data('criterium-id');
                        }
                    }
                });
            });
            if (r3turn) return;
            $('#links select').append($('<option></option>').attr('value', '-1').text('Не выбрано'));
            answers.forEach(function(el, i, a) {
                $('#links select').append($('<option></option>').attr('value', el.id).text(el.answer_name));
            });
            $.ajax({
                url: apiUrl + 'get.php?get=answers' + getParameter,
                success: function(data, textStatus, jqXHR) {
                    var answers = JSON.parse(data);
                    if (answers && answers[0].id) {
                        $('#links select').val(answers[0].id);
                    }
                }
            });
        });
        $('#links select').on('change', function(event) {
            var answerId = $(this).val();
            var postParameter = {
                section: 'links',
                method: 'add',
                answer_id: answerId
            };
            var r3turn = false;
            categories.forEach(function(currentValue, index, array) {
                var curCatId = currentValue.id;
                if (!$('input[data-category-id="' + curCatId + '"]:checked').val() && $('input[data-category-id="' + curCatId + '"]').parents('.hidden').length === 0) {
                    // util.showErrorMessage('Необходимо выбрать вариант в каждой секции');
                    r3turn = true;
                }
                $('input[data-category-id="' + curCatId + '"]').each(function(i) {
                    if ($(this).parents('.hidden').length === 0) { // Если не скрыт
                        if ($(this).prop('checked')) {
                            postParameter['cat_' + curCatId] = $(this).data('criterium-id');
                        }
                    }
                });
            });
            if (r3turn) return;
            $.ajax({
                url: apiUrl + 'post.php',
                type: 'POST',
                data: postParameter,
                success: function(data, textStatus, jqXHR) {
                    if (data === 'success') {
                        util.showSuccessMessage('Ответ изменён');
                        // $('a[href="#links"]').trigger('click');
                    }
                }
            });
        });
    }
});