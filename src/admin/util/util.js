import Mustache from 'mustache.js';

import messageTmpl from './message.tmpl';

export var apiUrl = '../api/';
var alertsCounter = 0;

export function showInfoMessage(text) {
    var message = {
        id: 'msg' + alertsCounter++,
        text: text,
        type: 'info'
    };
    $('.alerts-wrap').append(Mustache.render(messageTmpl, message));
    setTimeout(function() {
        $('#' + message.id).trigger('click');
    }, 3000);
}

export function showSuccessMessage(text) {
    var message = {
        id: 'msg' + alertsCounter++,
        text: text,
        type: 'success'
    };
    $('.alerts-wrap').append(Mustache.render(messageTmpl, message));
    setTimeout(function() {
        $('#' + message.id).trigger('click');
    }, 3000);
}

export function showErrorMessage(text) {
    var message = {
        id: 'msg' + alertsCounter++,
        text: text,
        type: 'danger'
    };
    $('.alerts-wrap').append(Mustache.render(messageTmpl, message));
    setTimeout(function() {
        $('#' + message.id).trigger('click');
    }, 3000);
}

export function clearAllData() {
    $('#categories tr').not(':first-child').remove();
    $('#criteria tr').not(':first-child').remove();
    $('#criteria select').off();
    $('#criteria option').remove();
    $('#answers-list').off();
    $('#answers option').remove();
    $('#answer-content').empty();
    $('#items-wrap').empty();
    $('#btnSaveAnswersItemsChanges').off();
    $('#items select').off();
    $('#items option').remove();
    $('#item-content').empty();
    $('#links tr').not(':first-child').remove();
    $('#links th:nth-last-child(n+3)').remove();
    $('#links select').empty();
    $('#links select').off();
    $('#links-wrap').empty();
    $('#links-meta').empty();
    $('#password-change form').trigger('reset');
    $('#selectBox').remove();
}