import Mustache from 'mustache.js';

import categoryTmpl     from './category.tmpl';
import categoryRootTmpl from './category_root.tmpl';

import * as util from '../util/util.js';

var apiUrl = util.apiUrl;

$(function() {
    $('a[href="#categories"]').on('click', function(event) {
        util.clearAllData();
        $.ajax({
            url: apiUrl + 'get.php?get=categories',
            success: function(data, textStatus, jqXHR) {
                var categories = JSON.parse(data);
                if (!categories) {
                    util.showInfoMessage('Категории не найдены');
                    return;
                }
                categories.forEach(function(category, index, array) {
                    var categoryHtml = Mustache.render(
                        category.is_root === '1' ? categoryRootTmpl : categoryTmpl, 
                        category);
                    $('#categories tbody').append(categoryHtml);
                });
                $('.btn-set-root').not('.root').on('click', function(event) {
                    var answer = confirm('Назначить новую главную категорию?');
                    if (answer) {
                        $.ajax({
                            url: apiUrl + 'post.php',
                            type: 'POST',
                            data: {
                                section: 'categories',
                                method: 'set_root',
                                id: $(this).data('category-id')
                            },
                            success: function(data, textStatus, jqXHR) {
                                if (data === 'success') {
                                    util.showSuccessMessage('Назначена главная категория');
                                    util.showInfoMessage('Проверьте критерии и связи');
                                    $('a[href="#categories"]').trigger('click');
                                }
                            }
                        });
                    }
                });
                $('.btnEditCategory').on('click', function(event) {
                    var newCategoryName = prompt('Введите новое название для категории', '');
                    if (newCategoryName !== null && newCategoryName !== '') {
                        $.ajax({
                            url: apiUrl + 'post.php',
                            type: 'POST',
                            data: {
                                section: 'categories',
                                method: 'edit',
                                id: $(this).data('category-id'),
                                cat_name: newCategoryName
                            },
                            success: function(data, textStatus, jqXHR) {
                                if (data === 'success') {
                                    util.showSuccessMessage('Категория переименована');
                                    $('a[href="#categories"]').trigger('click');
                                }
                            }
                        });
                    }
                });
                $('.btnDeleteCategory').on('click', function(event) {
                    var answer = confirm('Удалить категорию?');
                    if (answer) {
                        $.ajax({
                            url: apiUrl + 'post.php',
                            type: 'POST',
                            data: {
                                section: 'categories',
                                method: 'delete',
                                id: $(this).data('category-id')
                            },
                            success: function(data, textStatus, jqXHR) {
                                if (data === 'success') {
                                    util.showSuccessMessage('Категория удалена');
                                    $('a[href="#categories"]').trigger('click');
                                }
                            }
                        });
                    }
                });
            }
        });
    });
    $('#btnAddCategory').on('click', function(event) {
        $.ajax({
            url: apiUrl + 'post.php',
            type: 'POST',
            data: {
                section: 'categories',
                method: 'add'
            },
            success: function(data, textStatus, jqXHR) {
                if (data === 'success') {
                    util.showSuccessMessage('Категория добавлена');
                    $('a[href="#categories"]').trigger('click');
                }
            }
        });
    });
    $('a[href="#categories"]').trigger('click');
});