import md5 from 'js-md5';

import * as util from '../util/util.js';

var apiUrl = util.apiUrl;

$(function() {
    $('#btnSaveChangedPassword').on('click', function(event) {
        var oldPassword = $('#oldPassword').val();
        var newPassword = $('#newPassword').val();
        var confirmedNewPassword = $('#confirmedNewPassword').val();
        if (newPassword !== confirmedNewPassword) {
            util.showErrorMessage('Подтверждённый пароль не совпадает с новым');
            return;
        }
        if (oldPassword === '' || newPassword === '' || confirmedNewPassword === '') {
            util.showErrorMessage('Необходимо заполнить все поля');
            return;
        }
        $.ajax({
            url: apiUrl + 'post.php',
            type: 'POST',
            data: {
                section: 'secured',
                o: md5(md5(oldPassword)),
                n: md5(md5(newPassword))
            },
            success: function(data, textStatus, jqXHR) {
                if (data === 'success') {
                    util.showSuccessMessage('Пароль изменён');
                    $('a[href="#password-change"]').trigger('click');
                } else if (data === 'error: wrong old hash') {
                    util.showErrorMessage('Старый пароль введён неверно');
                    $('a[href="#password-change"]').trigger('click');
                }
                $('#password-change form').trigger('reset');
            }
        });
    });
});