<?php
    
    define('MAIN', $_SERVER['DOCUMENT_ROOT'].'/');

    function get_saved_password_hash() {
        include('./../api/common.php');

        $sql = "SELECT `p_hash` FROM `secured`;";
        if (!$result = $mysqli->query($sql)) {
            return "NULL";
        }
        $row = $result->fetch_assoc();
        return $row["p_hash"];
    }

    function is_cookie_correct() {
        $cookie = $_COOKIE['ph'];
        if (!$cookie) return false;
        if (get_saved_password_hash() != $cookie) return false;
        setcookie('ph', $cookie, time() + 60 * 60 * 24 * 30);
        return true;
    }

    $saved_password_hash = get_saved_password_hash();
    $entered_password_hash = md5(md5($_POST['password']));

    if ($saved_password_hash == $entered_password_hash || is_cookie_correct()) {
        setcookie('ph', $saved_password_hash, time() + 60 * 60 * 24 * 30);
        include 'admin-panel.php';
    } else {
        include 'admin-login.php';
        if ($_POST['password'] && $saved_password_hash != $entered_password_hash) {
            echo '<div style="color: red; font-weight: bold; text-align: center;">Пароль неправильный!</div>';
        }
    }

?>