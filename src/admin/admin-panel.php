<?php
    if (!defined('MAIN')) {
        http_response_code(404);
        exit;
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Panel</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
	<link rel="stylesheet" href="font-awesome.min.css">
	
	<link rel="stylesheet" href="admin.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<div class="container">
		<h1>Админ. панель</h1>
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#categories">Настройка категорий</a></li>
			<li><a data-toggle="tab" href="#criteria">Настройка критериев</a></li>
			<li><a data-toggle="tab" href="#answers">Настройка ответов</a></li>
			<li><a data-toggle="tab" href="#items">Настройка товаров</a></li>
			<li><a data-toggle="tab" href="#links">Настройка формы</a></li>
			<li><a data-toggle="tab" href="#password-change">Настройка пароля</a></li>
		</ul>

		<div class="tab-content">
			<!-- Настройка категорий -->  
			<div id="categories" class="tab-pane fade in active">
				<h3>Настройка категорий <small><a href="#" id="btnAddCategory">Добавить новую</a></small></h3>
				<table class="table table-striped table-categories">
					<tbody>
						<tr>
							<th class="th-wide">Название</th>
							<th class="th-narrow">Главная</th>
							<th class="th-narrow">Изменить</th>
							<th class="th-narrow">Удалить</th>
						</tr>
					</tbody>  
				</table>
			</div>

			<!-- Настройка критериев --> 
			<div id="criteria" class="tab-pane fade criteria">
				<h3>Настройка критериев <small><a href="#" id="btnAddCriteria">Добавить новый</a></small></h3>
				<select class="form-control" style="font-family:Arial, FontAwesome;"></select>
				<table class="table table-striped table-criteria">
					<tbody>
						<tr>
							<th class="th-wide">Название</th>
							<th class="th-wide">Подкатегории</th>
							<th class="th-narrow">Изменить</th>
							<th class="th-narrow">Удалить</th>
						</tr>
					</tbody>  
				</table>
			</div>

			<!-- Настройка ответов --> 
			<div id="answers" class="tab-pane fade answers">
				<h3>Настройка ответов <small><a href="#" id="btnAddAnswer">Добавить новый</a></small></h3>
				<select id="answers-list" class="form-control"></select>
				<div id="answer-content"></div>
				<div id="items-wrap"></div>
				<button class="btn btn-lg btn-primary" type="button" id="btnSaveAnswersItemsChanges">Сохранить изменения</button>
			</div>

			<!-- Настройка товаров --> 
			<div id="items" class="tab-pane fade items">
				<h3>Настройка товаров <small><a href="#" id="btnAddItem">Добавить новый</a></small></h3>
				<select id="items-list" class="form-control"></select>
				<div id="item-content">
				</div>
			</div>

			<!-- Настройка связей --> 
			<div id="links" class="tab-pane fade links">
				<!-- <h3>Настройка формы <small><a href="#" id="btnAddLink">Добавить новую связь</a></small> <small>Для выбора варианта нажмите на ячейку таблицы</small></h3> -->
				<h3>Настройка формы</h3>
				<div id="links-meta"></div>
				<div id="links-wrap"></div>
				<div style="margin-bottom: 36px;">Выберите ответ: <select class="form-control"></select></div>
			</div>

			<!-- Настройка пароля --> 
			<div id="password-change" class="tab-pane fade">
				<h3>Настройка пароля</h3>
				<form class="form-change-password">
					<label for="inputPassword" class="sr-only">Старый пароль</label>
					<input type="password" id="oldPassword" class="form-control" placeholder="Старый пароль" required>
					<label for="inputPassword" class="sr-only">Новый пароль</label>
					<input type="password" id="newPassword" class="form-control" placeholder="Новый пароль" required>
					<label for="inputPassword" class="sr-only">Новый пароль ещё раз</label>
					<input type="password" id="confirmedNewPassword" class="form-control" placeholder="Новый пароль ещё раз" required>
					<button class="btn btn-lg btn-primary btn-block" type="button" id="btnSaveChangedPassword">Сохранить</button>
				</form>				
			</div>
		</div>

		<div class="alerts-wrap"></div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<script type="text/javascript" src="admin.js"></script>

</body>
</html>