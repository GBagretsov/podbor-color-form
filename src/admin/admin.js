import './categories/categories.js';
import './criteria/criteria.js';
import './answers/answers.js';
import './items/items.js';
import './links/links.js';
import './password/password.js';