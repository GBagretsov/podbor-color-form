import Sortable from 'Sortable';

import * as util from '../util/util.js';

var apiUrl = util.apiUrl;

$(function() {
    var curAnswerId;

    $('a[href="#answers"]').on('click', function(event) {
        util.clearAllData();
        $.ajax({
            url: apiUrl + 'get.php?get=answers',
            success: function(data, textStatus, jqXHR) {
                var answers = JSON.parse(data);
                if (!answers) {
                    util.showInfoMessage('Ответы не найдены');
                    return;
                }
                var select = $('#answers-list');
                $.each(answers, function(key, value) {
                    $(select).append($("<option></option>").val(value.id).text(value.answer_name));
                });
                $('#answers-list').on('change', function(event) {
                    getAnswer($(this).val());
                    curAnswerId = $(this).val();
                });
                if (curAnswerId === 'last') {
                    $('#answers option').last().prop('selected', true);
                    curAnswerId = $('#answers-list').val();
                } else if (curAnswerId && $('#answers-list option[value=' + curAnswerId + ']').length > 0) {
                    $("#answers-list").val(curAnswerId);
                } else {
                    curAnswerId = $('#answers-list').val();
                }
                $('#answers-list').trigger('change');
            }
        });
    });
    
    // Добавление ответа
    $('#btnAddAnswer').on('click', function(event) {
        $.ajax({
            url: apiUrl + 'post.php',
            type: 'POST',
            data: {
                section: 'answers',
                method: 'add'
            },
            success: function(data, textStatus, jqXHR) {
                if (data === 'success') {
                    curAnswerId = 'last';
                    util.showSuccessMessage('Ответ добавлен');
                    $('a[href="#answers"]').trigger('click');
                }
            }
        });
    });

    function getAnswer(answerId) {
        var answerContent = $('#answer-content');
        var itemsWrap = $('#items-wrap');
        $(answerContent).empty();
        $(itemsWrap).empty();
        $('#btnSaveAnswersItemsChanges').off();
        $.ajax({
            url: apiUrl + 'get.php?get=answers&id=' + answerId,
            success: function(data, textStatus, jqXHR) {
                var answer = JSON.parse(data)[0];
                var annotation = answer.annotation ? answer.annotation : '';
                var answerContentHtml = 
                    '<div>' + 
                        '<h4>Имя:</h4> ' + answer.answer_name + 
                        ' <span id="btnEditAnswerName" class="glyphicon glyphicon-pencil btn-edit" title="Изменить имя" aria-hidden="true"></span>' +
                        '<span id="btnDeleteAnswer" class="glyphicon glyphicon-trash btn-edit" title="Удалить ответ" aria-hidden="true"></span>' + 
                        '<a href="../answer/answer.php?id=' + answerId + '" target="_blank" id="btnGoToAnswerPage" style="margin-left: 6px;">Перейти на страницу ответа</a>' +
                        '<a id="btnCopyAnswer" style="cursor: pointer; margin-left: 14px;">Создать копию ответа</a>' +
                    '</div>' +
                    '<div><h4>Заголовок страницы ответа:</h4> ' + answer.heading + ' <span id="btnEditAnswerHeading" class="glyphicon glyphicon-pencil btn-edit" title="Изменить заголовок страницы ответа" aria-hidden="true"></span></div>' + 
                    '<div class="annotation-wrap"><h4>Аннотация к ответу:</h4></br>' + 
                    '<textarea name="annotation" cols="auto" rows="5">' + annotation + '</textarea><br>' +
                    '<a class="pull-right" id="btnEditAnswerAnnotation">Сохранить изменения</a>' +
                    '<a class="pull-right" id="btnInsertLink">Вставить ссылку</a>' +
                    '</div>';
                $(answerContent).append(answerContentHtml);
                $(answerContent).append('<h4>Список товаров</h4><br/>');
                $.ajax({
                    url: apiUrl + 'get.php?get=items',
                    type: 'GET',
                    success: function(data, textStatus, jqXHR) {
                        var items = JSON.parse(data);
                        if (!items) {
                            util.showInfoMessage('Товары не найдены');
                            return;
                        }
                        $.ajax({
                            url: apiUrl + 'get.php?get=answers_items',
                            type: 'GET',
                            success: function(data, textStatus, jqXHR) {
                                var answersItems = JSON.parse(data);
                                $.ajax({
                                    url: apiUrl + 'get.php?get=descriptions',
                                    type: 'GET',
                                    success: function(data, textStatus, jqXHR) {
                                        var descriptions = JSON.parse(data);
                                        items.sort(function(a, b) {
                                            var ai_a = answersItems.find(function(ai, i, arr) {
                                                return ai.answer_id + '' === answerId && ai.item_id === a.id;
                                            });
                                            var id_a = ai_a ? parseInt(ai_a.id) : Number.MAX_SAFE_INTEGER;

                                            var ai_b = answersItems.find(function(ai, i, arr) {
                                                return ai.answer_id + '' === answerId && ai.item_id === b.id;
                                            });
                                            var id_b = ai_b ? parseInt(ai_b.id) : Number.MAX_SAFE_INTEGER;

                                            if (id_a === id_b) { // Товар не отмечен
                                                if (parseInt(a.id) < parseInt(b.id)) {
                                                    return -1;
                                                }
                                                if (parseInt(a.id) > parseInt(b.id)) {
                                                    return 1;
                                                }
                                            }
                                            if (id_a < id_b) {
                                                return -1;
                                            }
                                            if (id_a > id_b) {
                                                return 1;
                                            }
                                            return 0;
                                        });
                                        items.forEach(function(curItem, index, array) {
                                            $(itemsWrap).append('<div><span class="glyphicon glyphicon-resize-vertical" aria-hidden="true" title="Переместить товар"></span><label><input type="checkbox" data-answer-id="' + answerId + '" data-item-id="' + curItem.id + '"> ' + curItem.item_name + '</label></div>');
                                        });
                                        $('#items-wrap input[type="checkbox"]').change(function(event) {
                                            if ($(this).prop('checked')) {
                                                var itemId = $(this).data('item-id') + '';
                                                $(this).parent().after('<select data-item-id="' + itemId + '" class="form-control descriptions-select"></select>');
                                                var select = $(this).parent().next();
                                                descriptions
                                                    .filter(function (d, i, a) {
                                                        return d.item_id + '' === itemId;
                                                    })
                                                    .forEach(function (d, i, a) {
                                                        $(select).append($('<option></option>').val(d.id).text(d.desc_name));
                                                    });
                                                if (!answersItems) return;
                                                var selectedDesc = answersItems.find(function(ai, i, a) {
                                                    return ai.answer_id + '' === answerId && ai.item_id === itemId;
                                                });
                                                if (!selectedDesc) return;
                                                var selectedDescId = selectedDesc.desc_id;
                                                $(select).val(selectedDescId);
                                            } else {
                                                $(this).parent().next().remove();
                                            }
                                        });
                                        if (answersItems) {
                                            answersItems.filter(function(ai, index, arr){
                                                return ai.answer_id === answerId;
                                            }).forEach(function(ai, index, arr) {
                                                var itemId = ai.item_id;
                                                $('#items-wrap input[data-item-id=' + itemId + ']').prop('checked', true);
                                                $('#items-wrap input[data-item-id=' + itemId + ']').trigger('change');
                                            });
                                        }
                                        Sortable.create($('#items-wrap')[0], {
                                            handle: '.glyphicon',
                                            animation: 150
                                        });
                                        $('#btnSaveAnswersItemsChanges').click(function(event) {
                                            var itemsIdsFormatted = '';
                                            var descsIdsFormatted = '';
                                            $('#items-wrap input:checked').each(function(i) {
                                                itemsIdsFormatted += $(this).data('item-id') + ' ';
                                                descsIdsFormatted += $(this).parent().next().val() + ' ';
                                            });
                                            itemsIdsFormatted = itemsIdsFormatted.trim();
                                            descsIdsFormatted = descsIdsFormatted.trim();
                                            if (itemsIdsFormatted === '') itemsIdsFormatted = '-1';
                                            if (descsIdsFormatted === '') descsIdsFormatted = '-1';
                                            $.ajax({
                                                url: apiUrl + 'post.php',
                                                type: 'POST',
                                                data: {
                                                    section: 'answers_items',
                                                    method: 'set',
                                                    answer_id: answerId,
                                                    items_ids: itemsIdsFormatted,
                                                    descs_ids: descsIdsFormatted
                                                },
                                                success: function(data, textStatus, jqXHR) {
                                                    if (data === 'success') {
                                                        util.showSuccessMessage('Товары для ответа установлены');
                                                        $('a[href="#answers"]').trigger('click');
                                                    }
                                                }
                                            });
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
                // Изменение имени
                $('#btnEditAnswerName').on('click', function(event) {
                    var newAnswerName = prompt('Введите новое название для ответа', '');
                    if (newAnswerName !== null && newAnswerName !== '') {
                        $.ajax({
                            url: apiUrl + 'post.php',
                            type: 'POST',
                            data: {
                                section: 'answers',
                                method: 'edit',
                                id: answer.id,
                                answer_name: newAnswerName
                            },
                            success: function(data, textStatus, jqXHR) {
                                if (data === 'success') {
                                    util.showSuccessMessage('Ответ изменён');
                                    $('a[href="#answers"]').trigger('click');
                                }
                            }
                        });
                    }
                });
                // Изменение заголовка
                $('#btnEditAnswerHeading').on('click', function(event) {
                    var newAnswerHeading = prompt('Введите новый заголовок для страницы ответа', '');
                    if (newAnswerHeading !== null && newAnswerHeading !== '') {
                        $.ajax({
                            url: apiUrl + 'post.php',
                            type: 'POST',
                            data: {
                                section: 'answers',
                                method: 'edit',
                                id: answer.id,
                                heading: newAnswerHeading
                            },
                            success: function(data, textStatus, jqXHR) {
                                if (data === 'success') {
                                    util.showSuccessMessage('Заголовок ответа изменён');
                                    $('a[href="#answers"]').trigger('click');
                                }
                            }
                        });
                    }
                });
                // Изменение аннотации
                $('#btnEditAnswerAnnotation').click(function(event){
                    var textArea = $('#answer-content textarea');
                    var newAnnotation = textArea.val();
                    if (newAnnotation === '') newAnnotation = 'NULL';
                    $.ajax({
                        url: apiUrl + 'post.php',
                        type: 'POST',
                        data: {
                            section: 'answers',
                            method: 'edit',
                            id: answerId,
                            annotation: newAnnotation
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (data === 'success') {
                                util.showSuccessMessage('Аннотация к ответу изменена');
                                $('a[href="#answers"]').trigger('click');
                            }
                        }
                    });
                });
                // Ссылка в аннотации
                $('#btnInsertLink').click(function(event){
                    var textArea = $('#answer-content textarea');
                    var descContent = textArea.val();
                    descContent += '[http://ссылка*надпись на кнопке ссылки]';
                    textArea.val(descContent);
                });
                // Удаление ответа
                $('#btnDeleteAnswer').on('click', function(event) {
                    var question = confirm('Удалить ответ?');
                    if (question) {
                        $.ajax({
                            url: apiUrl + 'post.php',
                            type: 'POST',
                            data: {
                                section: 'answers',
                                method: 'delete',
                                id: answer.id
                            },
                            success: function(data, textStatus, jqXHR) {
                                if (data === 'success') {
                                    util.showSuccessMessage('Ответ удалён');
                                    util.showInfoMessage('Проверьте связи формы после удаления ответа');
                                    $('a[href="#answers"]').trigger('click');
                                }
                            }
                        });
                    }
                });
                // Создание копии ответа
                $('#btnCopyAnswer').on('click', function(event) {
                    $.ajax({
                        url: apiUrl + 'post.php',
                        type: 'POST',
                        data: {
                            section: 'answers',
                            method: 'copy',
                            id: answerId
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (data === 'success') {
                                util.showSuccessMessage('Ответ скопирован');
                                curAnswerId = 'last';
                                $('a[href="#answers"]').trigger('click');
                            }
                        }
                    });
                });
            }
        });
    }
});