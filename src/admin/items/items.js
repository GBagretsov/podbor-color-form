import * as util from '../util/util.js';

var apiUrl = util.apiUrl;

$(function() {
    var curItemId;

    $('a[href="#items"]').on('click', function(event) {
        util.clearAllData();
        $.ajax({
            url: apiUrl + 'get.php?get=items',
            success: function(data, textStatus, jqXHR) {
                var items = JSON.parse(data);
                if (!items) {
                    util.showInfoMessage('Товары не найдены');
                    return;
                }
                var select = $('#items select');
                if (curItemId === 'highest') {
                    curItemId = items[items.length - 1].id;
                }
                items.sort(function(a, b) {
                    var aName = a.item_name.toLowerCase();
                    var bName = b.item_name.toLowerCase();
                    if (aName < bName) {
                        return -1;
                    }
                    if (aName > bName) {
                        return 1;
                    }
                    // равные имена
                    return 0;
                });
                $.each(items, function(key, value) {
                    $(select).append($("<option></option>").val(value.id).text(value.item_name));
                });
                $('#items select').on('change', function(event) {
                    getItem($(this).val());
                    curItemId = $(this).val();
                });
                if (curItemId === 'last') {
                    $('#items option').last().prop('selected', true);
                    curItemId = $('#items select').val();
                } else if (curItemId && $('#items select option[value=' + curItemId + ']').length > 0) {
                    $("#items select").val(curItemId);
                } else {
                    curItemId = $('#items select').val();
                }
                $('#items select').trigger('change');
            }
        });
    });

    $('#btnAddItem').on('click', function(event) {
        $.ajax({
            url: apiUrl + 'post.php',
            type: 'POST',
            data: {
                section: 'items',
                method: 'add'
            },
            success: function(data, textStatus, jqXHR) {
                if (data === 'success') {
                    curItemId = 'highest';
                    util.showSuccessMessage('Товар добавлен');
                    $('a[href="#items"]').trigger('click');
                }
            }
        });
    });

    function getItem(itemId) {
        var itemWrap = $('#item-content');
        $(itemWrap).empty();
        $.ajax({
            url: apiUrl + 'get.php?get=items&id=' + itemId,
            type: 'GET',
            success: function(data, textStatus, jqXHR) {
                var items = JSON.parse(data);
                if (!items) {
                    util.showInfoMessage('Товары не найдены');
                    return;
                }
                $.ajax({
                    url: apiUrl + 'get.php?get=descriptions&item_id=' + itemId,
                    type: 'GET',
                    success: function(data, textStatus, jqXHR) {
                        var descriptions = JSON.parse(data);
                        if (!descriptions) {
                            util.showInfoMessage('Описания не найдены');
                            return;
                        }
                        items.forEach(function(curItem, index, array) {
                            var descHtml = '';
                            descriptions.forEach(function(d, i, a) {
                                var curDescName = d.desc_name;
                                var curDescContent = d.desc_content;
                                if (!curDescContent) curDescContent = '';
                                var curDescDelBtnHtml = '';
                                if (a.length > 1) {
                                    curDescDelBtnHtml = '<span class="glyphicon glyphicon-trash btn-edit btnDeleteDesc" data-desc-id="' + d.id + '" title="Удалить описание" aria-hidden="true"></span>';
                                }
                                descHtml += 
                                '<div style="margin-bottom: 14px;"><h5>Название описания:</h5> ' + curDescName + ' <span class="glyphicon glyphicon-pencil btn-edit btnEditDescName" data-desc-id="' + d.id + '" title="Изменить название описания" aria-hidden="true"></span>' + curDescDelBtnHtml +
                                '<textarea name="description" data-desc-id="' + d.id + '" cols="auto" rows="10">' + curDescContent + '</textarea><br>' +
                                '<a class="pull-right btnEditDescContent" data-desc-id="' + d.id + '">Сохранить изменения</a>' +
                                '<a class="pull-right btnInsertLink" data-desc-id="' + d.id + '">Вставить ссылку</a></div>';
                            });
                            var itemHtml = 
                                '<article class="item-card" data-item-id="' + curItem.id + '">' +
                                    '<div><h4>Название:</h4> ' + curItem.item_name + ' <span class="glyphicon glyphicon-pencil btn-edit btnEditItemName" data-item-id="' + curItem.id + '" title="Изменить название товара" aria-hidden="true"></span>' +
                                    '<span class="glyphicon glyphicon-trash btn-edit btnDeleteItem" data-item-id="' + curItem.id + '" title="Удалить товар" aria-hidden="true"></span></div>' +
                                    '<div><h4>Ссылка на товар:</h4> <a href="' + curItem.item_link + '" target="_blank">' + curItem.item_link + '</a> <span class="glyphicon glyphicon-pencil btn-edit btnEditItemLink" data-item-id="' + curItem.id + '" title="Изменить ссылку на товар" aria-hidden="true"></span></div>' +
                                    '<h4>Описания <small><a class="btnAddDesc">Добавить новое</a></small></h4><br>' + descHtml +
                                '</article>';
                            $(itemWrap).append(itemHtml);
                        });
                        /* Свойства товара */
                        $('.btnEditItemName').click(function(event){
                            // var itemId = $(this).data('item-id');
                            var newItemName = prompt('Введите новое название товара', '');
                            if (newItemName !== null && newItemName !== '') {
                                $.ajax({
                                    url: apiUrl + 'post.php',
                                    type: 'POST',
                                    data: {
                                        section: 'items',
                                        method: 'edit',
                                        id: itemId,
                                        item_name: newItemName
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                        if (data === 'success') {
                                            util.showSuccessMessage('Название товара изменено');
                                            $('a[href="#items"]').trigger('click');
                                        }
                                    }
                                });
                            }
                        });
                        $('.btnEditItemLink').click(function(event){
                            // var itemId = $(this).data('item-id');
                            var newItemLink = prompt('Введите новую ссылку на товар', '');
                            if (newItemLink !== null && newItemLink !== '') {
                                if (newItemLink.lastIndexOf('http:', 0) !== 0 && newItemLink.lastIndexOf('https:', 0) !== 0) {
                                    newItemLink = 'http://' + newItemLink;
                                }
                                $.ajax({
                                    url: apiUrl + 'post.php',
                                    type: 'POST',
                                    data: {
                                        section: 'items',
                                        method: 'edit',
                                        id: itemId,
                                        item_link: newItemLink
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                        if (data === 'success') {
                                            util.showSuccessMessage('Ссылка на товар изменена');
                                            $('a[href="#items"]').trigger('click');
                                        }
                                    }
                                });
                            }
                        });
                        $('.btnDeleteItem').click(function(event){
                            var question = confirm('Удалить товар?');
                            if (question) {
                                // var itemId = $(this).data('item-id');
                                $.ajax({
                                    url: apiUrl + 'post.php',
                                    type: 'POST',
                                    data: {
                                        section: 'items',
                                        method: 'delete',
                                        id: itemId
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                        if (data === 'success') {
                                            util.showSuccessMessage('Товар удалён');
                                            $('a[href="#items"]').trigger('click');
                                        }
                                    }
                                });
                            }
                        });
                        /* Свойства описания */
                        $('.btnAddDesc').on('click', function(event) {
                            $.ajax({
                                url: apiUrl + 'post.php',
                                type: 'POST',
                                data: {
                                    section: 'descriptions',
                                    method: 'add',
                                    item_id: itemId
                                },
                                success: function(data, textStatus, jqXHR) {
                                    if (data === 'success') {
                                        util.showSuccessMessage('Описание добавлено');
                                        $('a[href="#items"]').trigger('click');
                                    }
                                }
                            });
                        });
                        $('.btnEditDescName').click(function(event){
                            var descId = $(this).data('desc-id');
                            var newDescName = prompt('Введите новое название описания', '');
                            if (newDescName !== null && newDescName !== '') {
                                $.ajax({
                                    url: apiUrl + 'post.php',
                                    type: 'POST',
                                    data: {
                                        section: 'descriptions',
                                        method: 'edit',
                                        id: descId,
                                        desc_name: newDescName
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                        if (data === 'success') {
                                            util.showSuccessMessage('Название описания изменено');
                                            $('a[href="#items"]').trigger('click');
                                        }
                                    }
                                });
                            }
                        });
                        $('.btnEditDescContent').click(function(event){
                            var descId = $(this).data('desc-id');
                            var textArea = $('textarea[data-desc-id="' + descId + '"]');
                            var descContent = textArea.val();
                            if (descContent === '') descContent = 'NULL';
                            $.ajax({
                                url: apiUrl + 'post.php',
                                type: 'POST',
                                data: {
                                    section: 'descriptions',
                                    method: 'edit',
                                    id: descId,
                                    desc_content: descContent
                                },
                                success: function(data, textStatus, jqXHR) {
                                    if (data === 'success') {
                                        util.showSuccessMessage('Описание товара изменено');
                                        $('a[href="#items"]').trigger('click');
                                    }
                                }
                            });
                        });
                        $('.btnInsertLink').click(function(event){
                            var descId = $(this).data('desc-id');
                            var textArea = $('textarea[data-desc-id="' + descId + '"]');
                            var descContent = textArea.val();
                            descContent += '[http://ссылка*надпись на кнопке ссылки]';
                            textArea.val(descContent);
                        });
                        $('.btnDeleteDesc').click(function(event){
                            var question = confirm('Удалить описание?');
                            if (question) {
                                var descId = $(this).data('desc-id');
                                $.ajax({
                                    url: apiUrl + 'post.php',
                                    type: 'POST',
                                    data: {
                                        section: 'descriptions',
                                        method: 'delete',
                                        id: descId
                                    },
                                    success: function(data, textStatus, jqXHR) {
                                        if (data === 'success') {
                                            util.showSuccessMessage('Описание удалено');
                                            $('a[href="#items"]').trigger('click');
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    }
});