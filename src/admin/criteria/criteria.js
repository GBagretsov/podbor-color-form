import Mustache from 'mustache.js';

import selectOptionTmpl      from './select_option.tmpl';
import selectOptionRootTmpl  from './select_option_root.tmpl';
import criteriaSelectBoxTmpl from './criteria_select_box.tmpl';

import * as util from '../util/util.js';

var apiUrl = util.apiUrl;

$(function() {
    var curCatId;

    $('a[href="#criteria"]').on('click', function(event) {
        util.clearAllData();
        $.ajax({
            url: apiUrl + 'get.php?get=categories',
            success: function(data, textStatus, jqXHR) {
                var categories = JSON.parse(data);
                if (!categories) {
                    util.showInfoMessage('Категории не найдены');
                    return;
                }

                $.ajax({
                    url: apiUrl + 'get.php?get=subcategories',
                    success: function(data, textStatus, jqXHR) {
                        var subCategories = JSON.parse(data);
                        if (!subCategories) {
                            util.showInfoMessage('Подкатегории не найдены');
                        }

                        var select = $('#criteria select');
                        $.each(categories, function(key, category) {
                            $(select).append(Mustache.render(
                                category.is_root === '1' ? 
                                selectOptionRootTmpl : selectOptionTmpl,
                                category)
                            );
                        });
                        $('#criteria select').on('change', function(event) {
                            getCriteria($(this).val(), categories, subCategories);
                            curCatId = $(this).val();
                        });
                        if (curCatId && $('#criteria select option[value=' + curCatId + ']').length > 0) {
                            $("#criteria select").val(curCatId);
                        } else {
                            curCatId = $('#criteria select').val();
                        }
                        $('#criteria select').trigger('change');
                    }
                });
            }
        });
    });

    function getCriteria(categoryId, categories, subCategories) {
        $('#criteria tr').not(':first-child').remove();
        $.ajax({
            url: apiUrl + 'get.php?get=criteria&cat_id=' + categoryId,
            success: function(data, textStatus, jqXHR) {
                var criteria = JSON.parse(data);
                if (!criteria) {
                    util.showInfoMessage('Критерии не найдены');
                    return;
                }
                var rootCat = categories.find(function(cat, i, array) {
                    return cat.is_root === '1';
                });
                var rootCatId = rootCat ? rootCat.id : '-1';
                if (rootCatId === curCatId) {
                    var rootCrits = criteria.filter(function(crit, i, a) {
                        return crit.parent_crit_id === null;
                    });
                    rootCrits.forEach(function(crit, i, arr) {
                        var curCritChildren = criteria.filter(function(c, ii, aa) {
                            return crit.id === c.parent_crit_id;
                        });
                        if (curCritChildren.length > 0) {
                            $('#criteria tbody').append(getCriteriumHtml(crit, false, true, categories, subCategories));
                            curCritChildren.forEach(function(c, ii, aa) {
                                $('#criteria tbody').append(getCriteriumHtml(c, true, false, categories, subCategories));
                            });
                        } else {
                            $('#criteria tbody').append(getCriteriumHtml(crit, true, true, categories, subCategories));
                        }
                    });
                    $($('#criteria .th-wide')[1]).removeClass('hidden');
                } else {
                    criteria.forEach(function(currentValue, index, array) {
                        var criteriumHtml = '<tr><td>' + currentValue.crit_name + '</td><td class="hidden">&mdash;</td>' +
                            '<td class="btn-warning btnEditCriterium"  data-criterium-id="' + currentValue.id + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></td>' +
                            '<td class="btn-danger btnDeleteCriterium" data-criterium-id="' + currentValue.id + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td></tr>';
                        $('#criteria tbody').append(criteriumHtml);
                    });
                    $($('#criteria .th-wide')[1]).addClass('hidden');
                }
                $('#criteria .td-subcat').click(function(event) {
                    var td = $(this);
                    var selectBoxHtml = Mustache.render(criteriaSelectBoxTmpl, {
                        critId: td.data('criterium-id'),
                        subCategories: categories.filter(function(cat){
                            return cat.is_root !== '1';
                        })
                    });
                    $('.container').append(selectBoxHtml);
                    $('#selectBox').css({top: $(this).position().top, left: $(this).position().left});
                    $('#ok').on('click', function(event) {
                        var catIdsFormatted = '';
                        $('#selectBox input:checked').each(function(i) {
                            catIdsFormatted += $(this).data('category-id') + ' ';
                        });
                        catIdsFormatted = catIdsFormatted.trim();
                        if (catIdsFormatted === '') catIdsFormatted = '-1';
                        $.ajax({
                            url: apiUrl + 'post.php',
                            type: 'POST',
                            data: {
                                section: 'subcategories',
                                method: 'set',
                                crit_id: $(this).data('criterium-id'),
                                cat_ids: catIdsFormatted
                            },
                            success: function(data, textStatus, jqXHR) {
                                if (data === 'success') {
                                    util.showSuccessMessage('Подкатегории установлены');
                                    $('a[href="#criteria"]').trigger('click');
                                }
                            }
                        });
                    });
                    $('#close').on('click', function(event) {
                        $('#selectBox').fadeOut(200, function() {
                            $('#selectBox').remove();
                        });
                    });
                });
                $('.btnEditCriterium').on('click', function(event) {
                    var newCriteriumName = prompt('Введите новое название для критерия', '');
                    if (newCriteriumName !== null && newCriteriumName !== '') {
                        $.ajax({
                            url: apiUrl + 'post.php',
                            type: 'POST',
                            data: {
                                section: 'criteria',
                                method: 'edit',
                                id: $(this).data('criterium-id'),
                                crit_name: newCriteriumName
                            },
                            success: function(data, textStatus, jqXHR) {
                                if (data === 'success') {
                                    util.showSuccessMessage('Критерий переименован');
                                    $('a[href="#criteria"]').trigger('click');
                                }
                            }
                        });
                    }
                });
                $('.btnDeleteCriterium').on('click', function(event) {
                    var answer = confirm('Удалить критерий?');
                    if (answer) {
                        $.ajax({
                            url: apiUrl + 'post.php',
                            type: 'POST',
                            data: {
                                section: 'criteria',
                                method: 'delete',
                                id: $(this).data('criterium-id')
                            },
                            success: function(data, textStatus, jqXHR) {
                                if (data === 'success') {
                                    util.showSuccessMessage('Критерий удалён');
                                    util.showInfoMessage('Проверьте связи формы после удаления критерия');
                                    $('a[href="#criteria"]').trigger('click');
                                }
                            }
                        });
                    }
                });
                $('.btn-add-subwork').on('click', function(event) {
                    $.ajax({
                        url: apiUrl + 'post.php',
                        type: 'POST',
                        data: {
                            section: 'criteria',
                            method: 'add',
                            cat_id: curCatId,
                            parent_crit_id: $(this).data('criterium-id')
                        },
                        success: function(data, textStatus, jqXHR) {
                            if (data === 'success') {
                                util.showSuccessMessage('Подвид работы добавлен');
                                $('a[href="#criteria"]').trigger('click');
                            }
                        }
                    });
                });
            }
        });
    }

    function getCriteriumHtml(criterium, showSubCatsSelector, isRootCrit, categories, subCategories) {
        var subCategoriesHtml = '';
        if (showSubCatsSelector) {
            var subCategoriesNames = '';
            if (subCategories) {
                subCategories.filter(function(sc, ii, aa){
                    return sc.crit_id === criterium.id;
                }).forEach(function(sc, ii, aa) {
                    subCategoriesNames += categories.find(function(cat, i, arr) {
                        return sc.cat_id === cat.id;
                    }).cat_name + '<br/>';
                });
            }
            if (subCategoriesNames === '') subCategoriesNames = 'Не выбрано';
            subCategoriesHtml = '<td class="td-subcat" data-criterium-id="' + criterium.id + '">' + subCategoriesNames + '</td>';
        } else {
            subCategoriesHtml = '<td>&mdash;</td>';
        }
        var critNameHtml = '';
        if (isRootCrit) {
            critNameHtml = '<td>' + criterium.crit_name + ' <span class="glyphicon glyphicon-plus btn-add-subwork" data-criterium-id="' + criterium.id + '" title="Добавить подвид работы" aria-hidden="true"></span></td>';
        } else {
            critNameHtml = '<td style="padding-left:2.5em;">' + criterium.crit_name + '</td>';
        }
        return '<tr>' + critNameHtml + subCategoriesHtml +
                '<td class="btn-warning btnEditCriterium"  data-criterium-id="' + criterium.id + '"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></td>' +
                '<td class="btn-danger btnDeleteCriterium" data-criterium-id="' + criterium.id + '"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></td></tr>';
    }

    $('#btnAddCriteria').on('click', function(event) {
        $.ajax({
            url: apiUrl + 'post.php',
            type: 'POST',
            data: {
                section: 'criteria',
                method: 'add',
                cat_id: curCatId
            },
            success: function(data, textStatus, jqXHR) {
                if (data === 'success') {
                    util.showSuccessMessage('Критерий добавлен');
                    $('a[href="#criteria"]').trigger('click');
                }
            }
        });
    });
});