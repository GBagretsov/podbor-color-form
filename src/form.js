import Mustache from 'mustache.js';
import SmoothScroll from 'smooth-scroll.js';

import categoryTmpl          from './form_templates/category.tmpl';
import categoryRootTmpl      from './form_templates/category_root.tmpl';
import criteriumTmpl         from './form_templates/criterium.tmpl';
import criteriumRootWorkTmpl from './form_templates/criterium_root_work.tmpl';
import criteriumSubWorkTmpl  from './form_templates/criterium_sub_work.tmpl';
import criteriumNoRadioTmpl  from './form_templates/criterium_root_work_no_radio.tmpl';

var apiUrl = 'api/';

var categories;

$.ajax({
    url: apiUrl + 'get.php?get=categories',
    success: function(data, textStatus, jqXHR) {
        categories = JSON.parse(data);
        /*
        var rootCategoryIndex = categories.findIndex(function (cat, i, a) {
            return cat.is_root === '1';
        });
        */
        var rootCategoryIndex = -1;
        for (var i = 0; i < categories.length; ++i) {
            if (categories[i].is_root === '1') {
                rootCategoryIndex = i;
                break;
            }
        }
        if (rootCategoryIndex > -1) {
            var temp = categories[rootCategoryIndex];
            categories.splice(rootCategoryIndex, 1);
            categories.unshift(temp);
        }
        categories.forEach(function(currentValue, index, array) {
            var categoryHtml = Mustache.render(index ? categoryTmpl : categoryRootTmpl, currentValue);
            $('#form').append(categoryHtml);
        });
        $.ajax({
            url: apiUrl + 'get.php?get=criteria',
            success: function(data, textStatus, jqXHR) {
                var criteria = JSON.parse(data);
                /*
                var rootCat = categories.find(function(cat, i, array) {
                    return cat.is_root === '1';
                });
                */
                var rootCat;
                for (var i = 0; i < categories.length; ++i) {
                    if (categories[i].is_root === '1') {
                        rootCat = categories[i];
                        break;
                    }
                }
                var rootCatId = rootCat ? rootCat.id : '-1';
                categories.forEach(function(cat) {
                    var criteriaForCurCat = criteria.filter(function(crit) {
                        return crit.cat_id === cat.id;
                    });
                    if (rootCatId === cat.id) {
                        var rootCrits = criteriaForCurCat.filter(function(crit) {
                            return crit.parent_crit_id === null;
                        });
                        rootCrits.forEach(function(crit) {
                            var curCritChildren = criteriaForCurCat.filter(function(c) {
                                return crit.id === c.parent_crit_id;
                            });
                            if (curCritChildren.length > 0) {
                                $('#rootCat>div').append(Mustache.render(criteriumNoRadioTmpl, crit));
                                curCritChildren.forEach(function(c) {
                                    $('#rootCat>div').append(Mustache.render(criteriumSubWorkTmpl, c));
                                });
                            } else {
                                $('#rootCat>div').append(Mustache.render(criteriumRootWorkTmpl, crit));
                            }
                        });
                    } else {
                        criteriaForCurCat.forEach(function(currentValue) {
                            var critHtml = Mustache.render(criteriumTmpl, currentValue);
                            $('.col-xs-12 div[data-category-id="' + currentValue.cat_id + '"]').append(critHtml);
                        });
                    }
                });

                $.ajax({
                    url: apiUrl + 'get.php?get=subcategories',
                    success: function(data, textStatus, jqXHR) {
                        var subCategories = JSON.parse(data);
                        $('#rootCat input').click(function (event) {
                            $('.col-xs-12').not('#rootCat').addClass('hidden');
                            var input = $(this);
                            subCategories
                                .filter(function (sc, i, a) {
                                    return sc.crit_id === input.data('criterium-id').toString();
                                })
                                .forEach(function (sc, i, a) {
                                    $('div[data-category-id=' + sc.cat_id + ']').parent().removeClass('hidden');
                                });
                            var anchor = document.querySelector('#rootCat hr');
                            var options = { 
                                speed: 800,
                                easing: 'easeInOutQuad'
                            };
                            new SmoothScroll().animateScroll(anchor, 0, options);
                        });
                        $('#form').removeClass('hidden');
                    }
                });
            }
        });
    }
});

$('#btnShowAnswer').on('click', function(event) {
    var answerNode = $('#answer');
    $(this).text('Загрузка...');
    var getParameter = '';
    var r3turn = false;
    categories.forEach(function(currentValue, index, array) {
        var curCatId = currentValue.id;
        if (!$('input[data-category-id="' + curCatId + '"]:checked').val() && $('input[data-category-id="' + curCatId + '"]').parents('.hidden').length === 0) {
            $('#btnShowAnswer').text('Показать ответ');
            answerNode.empty();
            answerNode.append('<p>Необходимо выбрать вариант в каждой секции!</p>');
            answerNode.removeClass('hidden');
            r3turn = true;
        }
        $('input[data-category-id="' + curCatId + '"]').each(function(i) {
            if ($(this).parents('.hidden').length === 0) { // Если не скрыт
                if ($(this).prop('checked')) {
                    getParameter += '&cat_' + curCatId + '=' + $(this).data('criterium-id');
                }
            }
        });
    });
    if (r3turn) return;
    $.ajax({
        url: apiUrl + 'get.php?get=answers' + getParameter,
        success: function(data, textStatus, jqXHR) {
            var answers = JSON.parse(data);
            answerNode.empty();
            if (answers && answers[0].id) {
                window.location.href = 'answer/answer.php?id=' + answers[0].id;
            } else {
                answerNode.append('<p>Нет подходящих ответов</p>');
            }
            answerNode.removeClass('hidden');
            $('#btnShowAnswer').text('Показать ответ');
        }
    });
});
