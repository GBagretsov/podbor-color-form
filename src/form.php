<?php
    include('./api/common.php');

    $sql = "SELECT * FROM `meta`;";
    if (!$result = $mysqli->query($sql)) {
        http_response_code(404);
        exit;
    }
    while ($row = $result->fetch_assoc()) {
        if ($row["meta_key"] == "form_heading")    $heading    = $row["meta_value"];
        if ($row["meta_key"] == "form_annotation") $annotation = $row["meta_value"];
    } 

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $heading; ?></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="stylesheet" href="form.css">
</head>
<body>
    <div class="container">
        <div class="row h1-wrap"><h1><?php echo $heading; ?></h1><?php if ($annotation) echo "<p>$annotation</p>"; ?></div>
        <div class="row hidden" id="form"></div>
        <div class="row text-center"><button type="button" class="btn btn-lg btn-warning" id="btnShowAnswer">Показать ответ</button></div>
        <div class="row text-center"><div class="answer" id="answer"></div></div>
      
        <script type="text/javascript">
            document.write("<a class='live-internet' href='//www.liveinternet.ru/click' "+
            "target=_blank><img src='//counter.yadro.ru/hit?t11.6;r"+
            escape(document.referrer)+((typeof(screen)=="undefined")?"":
            ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
            screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
            ";"+Math.random()+
            "' alt='' title='LiveInternet: показано число просмотров за 24"+
            " часа, посетителей за 24 часа и за сегодня' "+
            "border='0' width='88' height='31'><\/a>")
        </script>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="form.js"></script>

</body>
</html>