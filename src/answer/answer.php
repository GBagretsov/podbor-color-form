<?php
    include('../api/common.php');

    $id = $_GET["id"];
    if (!$id) {
        http_response_code(404);
        exit;
    }

    $sql = "SELECT * FROM `items`;";
    if (!$result = $mysqli->query($sql)) {
        http_response_code(404);
        exit;
    }
    while ($row = $result->fetch_assoc()) {
        $items[] = $row;
    }

    $sql = "SELECT * FROM `answers` WHERE id=$id;";
    if (!$result = $mysqli->query($sql)) {
        http_response_code(404);
        exit;
    }    
    while ($row = $result->fetch_assoc()) {
        $heading    = $row["heading"];
        $annotation = $row["annotation"];
    }

    $sql = "SELECT * FROM `descriptions`;";
    if (!$result = $mysqli->query($sql)) {
        http_response_code(404);
        exit;
    }
    while ($row = $result->fetch_assoc()) {
        $descriptions[] = $row;
    }

    $sql = "SELECT * FROM `answers_items` WHERE answer_id=".$id.";";
    if (!$result = $mysqli->query($sql)) {
        http_response_code(404);
        exit;
    }
    while ($row = $result->fetch_assoc()) {
        $answers_items[] = $row;
    }

    function ParseLinks($template='')
    {
        $result = $template;
        $pattern = '/\[.+\*.+\]/iU';
        preg_match_all($pattern, $result, $matches, PREG_OFFSET_CAPTURE);
        foreach ($matches[0] as $match) {
            $params = explode('*', $match[0]);
            $params[0] = str_replace('[', '', $params[0]);
            $params[1] = str_replace(']', '', $params[1]);
            if (substr($params[0], 0, 7) !== "http://" && substr($params[0], 0, 8) !== "https://") {
                $params[0] = 'http://'.$params[0];
            }
            $a_tag = '<a href="'.$params[0].'" target="_blank">'.$params[1].'</a>';
            $result = str_replace($match[0], $a_tag, $result);
        }
        return $result;
    }

    ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo $heading; ?></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <link rel="stylesheet" href="answer.css">
</head>
<body>
    <div class="container">
        <div class="row h1-wrap"><h1><?php echo $heading; ?> <span id="back" class="back"><a href="">Вернуться назад</a></span></h1><?php if ($annotation) echo "<p>".ParseLinks($annotation)."</p>"; ?></div>
        <div class="row">
            <?php foreach ($answers_items as $ai) { 

                $item = null;
                foreach($items as $i) {
                    if ($ai["item_id"] == $i["id"]) {
                        $item = $i;
                        break;
                    }
                } 

                $item_description = null;
                foreach($descriptions as $d) {
                    if ($ai["desc_id"] == $d["id"]) {
                        $item_description = $d["desc_content"];
                        break;
                    }
                } 

                $item_description = ParseLinks($item_description);
                ?>

                <div class="col-md-12">
                    <article>
                        <h2><?php echo $item["item_name"] ?> <small><a href=<?php echo $item["item_link"] ?> target="_blank">Перейти на страницу с товаром</a></small></h2>
                        <?php if ($item_description != NULL) { ?>
                            <p><?php echo $item_description ?></p>
                        <?php } ?>
                    </article>
                </div>
                <hr/>

            <?php } ?>
        </div>
    </div>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="answer.js"></script>
</body>
</html>
