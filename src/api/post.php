<?php
    include('common.php');

    // API:

    // POST:

    // section=categories
    // -- method=add
    // -- method=edit:   id, cat_name
    // -- method=delete: id

    // section=criteria
    // -- method=add:    cat_id
    // -- method=edit:   id, crit_name
    // -- method=delete: id

    // section=subcategories
    // -- method=set:    crit_id, cat_ids

    // section=answers
    // -- method=add
    // -- method=edit:   id, answer_name, heading, annotation
    // -- method=copy:   id
    // -- method=delete: id

    // section=items
    // -- method=add
    // -- method=edit:   id, item_name, item_link
    // -- method=delete: id

    // section=descriptions
    // -- method=add:    item_id
    // -- method=edit:   id, desc_name, desc_content
    // -- method=delete: id

    // section=answers_items
    // -- method=set:    answer_id, items_ids, descs_ids

    // section=links
    // -- method=add
    // -- method=add:    answer_id, cat_id=crit_ids
    // -- method=edit:   id, cat_id=crit_ids
    // -- method=delete: id

    // section=secured

    // section=meta
    // -- method=edit:    meta_key, meta_value

    $section = $_POST["section"];

    if ($section == "categories") {

        $method = $_POST["method"];

        if ($method == "add") {
            $mysqli->autocommit(FALSE);
            $mysqli->query("INSERT INTO categories (id, cat_name) VALUES (NULL,  'Новая категория');");
            $mysqli->query("SET @new_id = CONCAT('cat_', (SELECT MAX(id) FROM categories));");
            $mysqli->query("SET @s = CONCAT('ALTER TABLE links ADD COLUMN `',@new_id,'` INT(11) NULL DEFAULT NULL');");
            $mysqli->query("PREPARE stmt1 FROM @s;");
            $mysqli->query("EXECUTE stmt1;");
            $mysqli->query("DEALLOCATE PREPARE stmt1;");
            if (!$mysqli->commit()) {
                echo "error";
                exit();
            }
            $mysqli->autocommit(TRUE);
            echo "success";
        } else if ($method == "set_root") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }
            
            $mysqli->autocommit(FALSE);
            $mysqli->query("UPDATE categories SET is_root = FALSE");
            $mysqli->query("UPDATE categories SET is_root = TRUE WHERE id='".$id."'");
            $mysqli->query("TRUNCATE subcategories");
            if (!$mysqli->commit()) {
                echo "error";
                exit();
            }
            $mysqli->autocommit(TRUE);

            echo "success";
        } else if ($method == "edit") {
            $id = $_POST["id"];
            $cat_name = $_POST["cat_name"];

            if (!$id || !$cat_name) {
                echo "error";
                exit;
            }
            
            $sql = "UPDATE categories SET cat_name='".$cat_name."' WHERE id='".$id."'";
            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }
            echo "success";
        } else if ($method == "delete") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }

            $sql = "SELECT id FROM categories WHERE is_root = TRUE";
            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }
            $root_id = $result->fetch_assoc()["id"];
            
            $mysqli->autocommit(FALSE);
            $mysqli->query("ALTER TABLE links DROP cat_".$id.";");
            $mysqli->query("DELETE FROM categories WHERE id='".$id."';");
            if ($root_id === $id) {
                $mysqli->query("TRUNCATE subcategories");
            }
            if (!$mysqli->commit()) {
                echo "error";
                exit();
            }
            $mysqli->autocommit(TRUE);

            echo "success";
        } else {
            echo "error";
        }
    } else if ($section == "criteria") {

        $method = $_POST["method"];
        
        if ($method == "add") {
            $cat_id = $_POST["cat_id"];
            if (!$cat_id) {
                echo "error";
                exit;
            }

            $parent_crit_id = $_POST["parent_crit_id"];
            if ($parent_crit_id) {
                $mysqli->autocommit(FALSE);
                $mysqli->query("INSERT INTO criteria (id, cat_id, crit_name, parent_crit_id) VALUES (NULL, $cat_id, 'Новый критерий', $parent_crit_id);");
                $mysqli->query("DELETE FROM subcategories WHERE crit_id=$parent_crit_id;");
                if (!$mysqli->commit()) {
                    echo "error";
                    exit();
                }
                $mysqli->autocommit(TRUE);

            } else {
                $sql = "INSERT INTO criteria (id, cat_id, crit_name) VALUES (NULL, $cat_id, 'Новый критерий');";
                if (!$result = $mysqli->query($sql)) {
                    echo "error";
                    exit;
                }
            }

            echo "success";
        } else if ($method == "edit") {
            $id = $_POST["id"];
            $crit_name = $_POST["crit_name"];

            if (!$id || !$crit_name) {
                echo "error";
                exit;
            }
            
            $sql = "UPDATE criteria SET crit_name='".$crit_name."' WHERE id='".$id."'";
            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }
            echo "success";
        } else if ($method == "delete") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }

            $mysqli->autocommit(FALSE);
            $mysqli->query("SET @c = (SELECT cat_id FROM criteria WHERE id='".$id."');");
            $mysqli->query("DELETE FROM criteria WHERE id='".$id."';");
            $mysqli->query("SET @s1 = CONCAT('UPDATE links SET `cat_', @c ,'` = NULL WHERE `cat_', @c, '` LIKE \"% ".$id." %\"');"); // Середина
            $mysqli->query("PREPARE stmt1 FROM @s1;");
            $mysqli->query("EXECUTE stmt1;");
            $mysqli->query("DEALLOCATE PREPARE stmt1;");
            $mysqli->query("SET @s2 = CONCAT('UPDATE links SET `cat_', @c ,'` = NULL WHERE `cat_', @c, '` LIKE \"".$id." %\"');");   // Начало
            $mysqli->query("PREPARE stmt2 FROM @s2;");
            $mysqli->query("EXECUTE stmt2;");
            $mysqli->query("DEALLOCATE PREPARE stmt2;");
            $mysqli->query("SET @s3 = CONCAT('UPDATE links SET `cat_', @c ,'` = NULL WHERE `cat_', @c, '` LIKE \"% ".$id."\"');");   // Конец
            $mysqli->query("PREPARE stmt3 FROM @s3;");
            $mysqli->query("EXECUTE stmt3;");
            $mysqli->query("DEALLOCATE PREPARE stmt3;");
            $mysqli->query("SET @s4 = CONCAT('UPDATE links SET `cat_', @c ,'` = NULL WHERE `cat_', @c, '` = \"".$id."\"');");   // Когда только один критерий
            $mysqli->query("PREPARE stmt4 FROM @s4;");
            $mysqli->query("EXECUTE stmt4;");
            $mysqli->query("DEALLOCATE PREPARE stmt4;");
            if (!$mysqli->commit()) {
                echo "error";
                exit();
            }
            $mysqli->autocommit(TRUE);

            echo "success";
        } else {
            echo "error";
        }
    } else if ($section == "subcategories") {
        $method = $_POST["method"];
        
        if ($method == "set") {
            
            $crit_id = $_POST["crit_id"];
            if (!$crit_id) {
                echo "error";
                exit;
            }

            $cat_ids = $_POST["cat_ids"];
            if (!$cat_ids) {
                echo "error";
                exit;
            }

            $cat_ids = explode(' ', $cat_ids);
            
            $mysqli->autocommit(FALSE);
            $mysqli->query("DELETE FROM subcategories WHERE crit_id=$crit_id");
            foreach ($cat_ids as $cat_id) {
                $mysqli->query("INSERT INTO subcategories (id, cat_id, crit_id) VALUES (NULL, $cat_id, $crit_id)");
            }

            if (!$mysqli->commit()) {
                echo "error";
                exit();
            }
            $mysqli->autocommit(TRUE);

            echo "success";
        } else {
            echo "error";
        }
    } else if ($section == "answers") {
        $method = $_POST["method"];

        if ($method == "add") {
            $sql = "INSERT INTO answers (id, answer_name, heading) VALUES (NULL,  'Новый ответ', 'Заголовок');";
            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }
            echo "success";
        } else if ($method == "edit") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }
            
            $answer_name = $_POST["answer_name"];
            if ($answer_name) {
                $sql = "UPDATE answers SET answer_name='".$answer_name."' WHERE id='".$id."'";
                if (!$result = $mysqli->query($sql)) {
                    echo "error";
                    exit;
                }                
            }

            $heading = $_POST["heading"];
            if ($heading) {
                $sql = "UPDATE answers SET heading='".$heading."' WHERE id='".$id."'";
                if (!$result = $mysqli->query($sql)) {
                    echo "error";
                    exit;
                }
            }

            $annotation = $_POST["annotation"];
            if ($annotation) {
                $sql = "UPDATE answers SET annotation='".$annotation."' WHERE id='".$id."'";
                $sql = str_replace("'NULL'", "NULL", $sql);
                if (!$result = $mysqli->query($sql)) {
                    echo "error";
                    exit;
                }
            }

            echo "success";
        } else if ($method == "copy") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }

            $mysqli->autocommit(FALSE);

            $mysqli->query("INSERT INTO answers (id, answer_name, heading, annotation) SELECT NULL, CONCAT('Копия ', answer_name), heading, annotation FROM answers WHERE id = $id;");
            $mysqli->query("INSERT INTO answers_items (id, answer_id, item_id, desc_id) SELECT NULL, MAX(answers.id), item_id, desc_id FROM answers JOIN answers_items WHERE answer_id = $id GROUP BY item_id ORDER BY answers_items.id");
            
            if (!$mysqli->commit()) {
                echo "error";
                exit();
            }
            $mysqli->autocommit(TRUE);

            echo "success";
        } else if ($method == "delete") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }
            
            $sql = "DELETE FROM answers WHERE id='".$id."';";

            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }

            echo "success";
        } else {
            echo "error";
        }
    } else if ($section == "items") {
        
        $method = $_POST["method"];

        if ($method == "add") {

            $mysqli->autocommit(FALSE);

            $mysqli->query("INSERT INTO items (item_name) VALUES ('Новый товар');");
            $mysqli->query("INSERT INTO descriptions (item_id, desc_name) VALUES ((SELECT MAX(id) FROM items), 'Новое описание');");

            if (!$mysqli->commit()) {
                echo "error";
                exit();
            }            
            $mysqli->autocommit(TRUE);
            echo "success";
        } else if ($method == "edit") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }
            
            $item_name = $_POST["item_name"];
            if ($item_name) {
                $sql = "UPDATE items SET item_name='".$item_name."' WHERE id='".$id."'";
                if (!$result = $mysqli->query($sql)) {
                    echo "error";
                    exit;
                }                
            }

            $item_link = $_POST["item_link"];
            if ($item_link) {
                $sql = "UPDATE items SET item_link='".$item_link."' WHERE id='".$id."';";
                if (!$result = $mysqli->query($sql)) {
                    echo "error";
                    exit;
                }                
            }

            echo "success";
        } else if ($method == "delete") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }
            
            $sql = "DELETE FROM items WHERE id='".$id."';";

            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }

            echo "success";
        } else {
            echo "error";
        }
    } else if ($section == "descriptions") {
        $method = $_POST["method"];

        if ($method == "add") {
            $item_id = $_POST["item_id"];

            if (!$item_id) {
                echo "error";
                exit;
            }

            $sql = "INSERT INTO descriptions (item_id, desc_name) VALUES ($item_id, 'Новое описание');";
            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }
            echo "success";
        } else if ($method == "edit") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }
            
            $desc_name = $_POST["desc_name"];
            if ($desc_name) {
                $sql = "UPDATE descriptions SET desc_name='".$desc_name."' WHERE id='".$id."'";
                if (!$result = $mysqli->query($sql)) {
                    echo "error";
                    exit;
                }                
            }

            $desc_content = $_POST["desc_content"];
            if ($desc_content) {
                $sql = "UPDATE descriptions SET desc_content='".$desc_content."' WHERE id='".$id."';";
                $sql = str_replace("'NULL'", "NULL", $sql);
                if (!$result = $mysqli->query($sql)) {
                    echo "error";
                    exit;
                }                
            }

            echo "success";
        } else if ($method == "delete") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }
            
            $sql = "DELETE FROM descriptions WHERE id='".$id."';";

            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }

            echo "success";
        } else {
            echo "error";
        }
    } else if ($section == "answers_items") {
        $method = $_POST["method"];
        
        if ($method == "set") {
            
            $answer_id = $_POST["answer_id"];
            if (!$answer_id) {
                echo "error";
                exit;
            }

            $items_ids = $_POST["items_ids"];
            if (!$items_ids) {
                echo "error";
                exit;
            }

            $items_ids = explode(' ', $items_ids);

            $descs_ids = $_POST["descs_ids"];
            if (!$descs_ids) {
                echo "error";
                exit;
            }

            $descs_ids = explode(' ', $descs_ids);
            
            $mysqli->autocommit(FALSE);
            $mysqli->query("DELETE FROM answers_items WHERE answer_id=$answer_id");
            for ($i = 0; $i < count($items_ids); $i++) {
                $mysqli->query("INSERT INTO answers_items (id, answer_id, item_id, desc_id) VALUES (NULL, $answer_id, $items_ids[$i], $descs_ids[$i])");
            }

            if (!$mysqli->commit()) {
                echo "error";
                exit();
            }
            $mysqli->autocommit(TRUE);

            echo "success";
        } else {
            echo "error";
        }
    } else if ($section == "links") {
        $method = $_POST["method"];

        if ($method == "add") {
            if (count($_POST) === 2) {
                $sql = "INSERT INTO links (answer_id) VALUES (NULL);";
            } else if (count($_POST) > 2) {
                $answer_id = $_POST["answer_id"];
                if ($answer_id === "-1") $answer_id = "NULL";
                $categories = array_keys($_POST);
                $sql = "SELECT id FROM links WHERE ";
                for ($i = 3; $i < count($_POST); $i++) {
                    $sql .= $categories[$i]." = '".$_POST[$categories[$i]]."' AND ";
                }
                $sql .= "1;";

                if (!$result = $mysqli->query($sql)) {
                    echo "error: cannot obtain id info";
                    exit;
                }
                $row = $result->fetch_assoc();
                $id = $row["id"];

                if ($id) {
                    $sql = "UPDATE links SET ";
                    for ($i = 3; $i < count($_POST); $i++) {
                        $sql .= $categories[$i]." = '".$_POST[$categories[$i]]."', ";
                    }
                    $sql .= "answer_id = $answer_id WHERE id = $id";
                } else {
                    $sql = "INSERT INTO links (";
                    for ($i = 3; $i < count($_POST); $i++) {
                        $sql .= $categories[$i].", ";
                    }
                    $sql .= "answer_id) VALUES (";
                    for ($i = 3; $i < count($_POST); $i++) {
                        $sql .= $_POST[$categories[$i]].", ";
                    }
                    $sql .= "$answer_id);";
                }
            }

            if (!$result = $mysqli->query($sql)) {
                echo "error: cannot insert/update";
                exit;
            }
            echo "success";
        } else if ($method == "edit") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }

            $col_name = array_keys($_POST)[3];
            $crits_or_answer_id  = $_POST[$col_name];
            
            $sql = "UPDATE links SET ".$col_name."='".$crits_or_answer_id."' WHERE id='".$id."'";
            $sql = str_replace("'NULL'", "NULL", $sql);
            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }
            echo "success";
        } else if ($method == "delete") {
            $id = $_POST["id"];

            if (!$id) {
                echo "error";
                exit;
            }
            
            $sql = "DELETE FROM links WHERE id='".$id."';";

            if (!$result = $mysqli->query($sql)) {
                echo "error";
                exit;
            }

            echo "success";
        } else {
            echo "error";
        }
    } else if ($section == "secured") {
        $old_hash = $_POST["o"];
        $new_hash = $_POST["n"];

        if (!$old_hash || !$new_hash) {
            echo "error: no params";
            exit;
        }

        $sql = "SELECT `p_hash` FROM `secured`;";
        if (!$result = $mysqli->query($sql)) {
            echo "error: cannot get hash";
            exit;
        }
        $row = $result->fetch_assoc();
        if ($row["p_hash"] != $old_hash) {
            echo "error: wrong old hash";
            exit;
        }
        $sql = "UPDATE `secured` SET p_hash = '".$new_hash."' WHERE p_hash = '".$old_hash."';";
        if (!$result = $mysqli->query($sql)) {
            echo "error: cannot update hash";
            exit;
        }
        echo "success";
    } else if ($section == "meta") {
        $method = $_POST["method"];

        if ($method == "edit") {
            $meta_key = $_POST["meta_key"];
            $meta_value = $_POST["meta_value"];

            if (!$meta_key || !$meta_value) {
                echo "error: no params";
                exit;
            }

            $sql = "UPDATE `meta` SET meta_value = '$meta_value' WHERE meta_key = '$meta_key';";
            $sql = str_replace("'NULL'", "NULL", $sql);
            if (!$result = $mysqli->query($sql)) {
                echo "error: cannot update meta";
                exit;
            }
            echo "success";
        } else {
            echo "error";
        }
    } else {
        echo "error: section not found";
    }
?>