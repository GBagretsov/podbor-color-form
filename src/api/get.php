<?php
    include('common.php');

    function sendResponse($mysqli, $sql) {
        if (!$result = $mysqli->query($sql)) {
            echo "error";
            exit;
        }

        while ($row = $result->fetch_assoc()) {
            $json[] = $row;
        }
        echo json_encode($json, JSON_UNESCAPED_UNICODE);
    }

    // API:

    // GET:

    // ?get=categories
    
    // ?get=criteria
    // ?get=criteria&cat=

    // ?get=subcategories
    
    // ?get=answers
    // ?get=answers&id=
    // ?get=answers&cat1=crits1&...catN=critsN
    
    // ?get=items
    // ?get=items&id=

    // ?get=descriptions
    // ?get=descriptions&item_id=
    
    // ?get=answers_items
    
    // ?get=links

    // ?get=meta

    $get = $_GET["get"];

    if ($get == "categories") {
        $sql = "SELECT * FROM categories";
        sendResponse($mysqli, $sql);
    }

    else if ($get == "criteria") {

        if (!$cat_id = $_GET["cat_id"]) {
            $sql = "SELECT * FROM criteria";
        } else {
            $sql = "SELECT * FROM criteria WHERE cat_id = ".$cat_id;
        }

        sendResponse($mysqli, $sql);
    }

    else if ($get == "subcategories") {
        $sql = "SELECT * FROM subcategories";
        sendResponse($mysqli, $sql);
    }

    else if ($get == "answers") {
        if (count($_GET) == 1) {
            $sql = "SELECT * FROM answers";
        } else if ($_GET["id"]) {
            $sql = "SELECT * FROM answers WHERE id='".$_GET["id"]."';";
        } else {
            $categories = array_keys($_GET);
            $sql = "SELECT answers.id AS id, answer_name FROM links JOIN answers ON links.answer_id = answers.id WHERE ";
            for ($i = 1; $i < count($_GET); $i++) {
                $sql .= $categories[$i]." = '".$_GET[$categories[$i]]."' AND ";
            }
            $sql .= "1;";
            $sql = str_replace("= 'NULL'", "IS NULL", $sql);
        }
        sendResponse($mysqli, $sql);
    }

    else if ($get == "items") {
        if (count($_GET) == 1) {
            $sql = "SELECT * FROM items";
        } else if ($_GET["id"]) {
            $sql = "SELECT * FROM items WHERE id='".$_GET["id"]."';";
        }
        sendResponse($mysqli, $sql);
    }

    else if ($get == "descriptions") {
        if (count($_GET) == 1) {
            $sql = "SELECT * FROM descriptions";
        } else if ($_GET["item_id"]) {
            $sql = "SELECT * FROM descriptions WHERE item_id='".$_GET["item_id"]."';";
        }
        sendResponse($mysqli, $sql);
    }

    else if ($get == "answers_items") {
        $sql = "SELECT * FROM answers_items";
        sendResponse($mysqli, $sql);
    }

    else if ($get == "links") {
        $sql = "SELECT * FROM links";
        sendResponse($mysqli, $sql);
    }

    else if ($get == "meta") {
        $sql = "SELECT * FROM meta";
        sendResponse($mysqli, $sql);
    }

    else {
        echo "error";
    }

?>