const path = require('path');
const BowerResolvePlugin = require('bower-resolve-webpack-plugin');

module.exports = {
  resolve: {
    plugins: [new BowerResolvePlugin()],
    modules: ['bower_components', 'node_modules'],
    descriptionFiles: ['bower.json', 'package.json'],
    mainFields: ['browser', 'main']
  },
  entry: {
    'form': './src/form.js',
    'answer/answer': './src/answer/answer.js',
    'admin/admin': './src/admin/admin.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.tmpl$/,
        use: 'raw-loader'
      }
    ]
  }
};