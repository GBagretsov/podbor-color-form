webpackConfig = require './webpack.config.js'

module.exports = (grunt) ->

  require('load-grunt-tasks')(grunt)
  
  # Project configuration.
  grunt.initConfig

    webpack:
      options:
        progress: false
      build: webpackConfig

    uglify:
      build:
        expand: true
        cwd: 'dist'
        src: '**/*.js'
        dest: 'dist'

    copy:
      files:
        expand: true
        cwd: 'src/'
        src: ['**', '!**/*.js', '!**/*.tmpl']
        dest: 'dist/'

    cssmin:
      target:
        files: [
          expand: true
          cwd: 'dist'
          src: '**/*.css'
          dest: 'dist'
        ]

    jshint:
      jshintrc: true
      options:
        esversion: 6
      src: ['src/**/*.js']

    clean: ['dist/*']

    php:
      dev:
        options:
          base: 'dist'
          hostname: 'localhost'
          port: 8000
          keepalive: true
          silent: false
          directives:
            error_reporting: '~E_ALL'

    watch:
      files: ['src/**/*']
      tasks: ['dev']
      options:
        livereload: true

    concurrent:
      dev:
        tasks: ['php', 'watch']
        options:
          logConcurrentOutput: true

  grunt.registerTask 'dev',  ['clean', 'jshint:src', 'copy', 'webpack']
  grunt.registerTask 'prod', ['dev', 'uglify', 'cssmin']

  grunt.registerTask 'default', ['dev', 'concurrent']

