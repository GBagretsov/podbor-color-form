﻿Установка скрипта:
1. Создать БД (MySQL). Запустить скрипт scheme.sql для создания таблиц.
2. Поместить на хостинг файлы из папки form, не меняя их относительное местоположение.
3. В файле form/api/common.php в 4 строке указать реквизиты БД (хост, пользователь, пароль, название БД).



Форма запускается по адресу: путь-к-папке/form.php
Админка запускается по адресу: путь-к-папке/admin/admin.php
Пароль по умолчанию - qwerty



Для разработки:
1. Установить PHP и MySQL Server. Прописать php в PATH
2. Установить расширение Live Reload
3. npm install, bower install
4. grunt/grunt dev/grunt prod
5. Перейти на http://localhost:8000